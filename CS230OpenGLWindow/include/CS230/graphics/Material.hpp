/*
 * Rudy Castan
 * Graphics Library
 * CS230
 */
#pragma once
#include <CS230/graphics/Color4f.hpp>
#include <CS230/math/matrix3.hpp>
#include <map>
#include <string>

namespace CS230
{
    class Shader;
    class Texture;
    struct [[nodiscard]] texture_uniform
    {
        const Texture* texture     = nullptr;
        int            textureSlot = 0;
    };

    struct [[nodiscard]] material
    {
        Shader*                                shader = nullptr;
        std::map<std::string, matrix3>         matrix3Uniforms{};
        std::map<std::string, Color4f>         color4fUniforms{};
        std::map<std::string, float>           floatUniforms{};
        std::map<std::string, texture_uniform> textureUniforms{};
    };
}
