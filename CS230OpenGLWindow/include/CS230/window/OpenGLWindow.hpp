/*
 * Rudy Castan
 * Windows Library
 * CS230
 */
#pragma once
#include <CS230/window/WindowSize.hpp>
#include <memory>

namespace CS230
{
    class WindowEventHandler;
    class PlatformWindow;

    class OpenGLWindow
    {
    public:
        OpenGLWindow() noexcept;
        ~OpenGLWindow() noexcept;

        enum FullScreen
        {
            FULLSCREEN,
            WINDOWED
        };
        bool CreateAndShowWindow(const char* title, WindowEventHandler* event_handler,
                                 window::size desired_size        = window::size{},
                                 FullScreen   start_fullscreen_as = WINDOWED) const noexcept;

        void PollEvents() const noexcept;
        bool ShouldQuit() const noexcept;
        void ShutDown() const noexcept;
        void SwapBackBuffer() const noexcept;

        void SetWindowTitle(const char* new_title) const noexcept;
        void TurnOnMonitorVerticalSynchronization(bool enable) const noexcept;
        bool IsMonitorVerticalSynchronizationOn() const noexcept;

        bool IsFullScreen() const noexcept;
        void ToggleFullScreen() const noexcept;

    public:
        OpenGLWindow(const OpenGLWindow&) = delete;
        OpenGLWindow& operator=(const OpenGLWindow&) = delete;
        OpenGLWindow(OpenGLWindow&&)                 = delete;
        OpenGLWindow& operator=(OpenGLWindow&&) = delete;

    private:
        std::unique_ptr<PlatformWindow> platformWindow{};
    };
}
