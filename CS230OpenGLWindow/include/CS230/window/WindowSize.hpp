/*
 * Rudy Castan
 * Windows Library
 * CS230
 */
#pragma once

namespace CS230
{
    namespace window
    {
        struct size
        {
            int width;
            int height;
        };

        inline bool operator==(const size& lhs, const size& rhs)
        {
            return lhs.width == rhs.width && lhs.height == rhs.height;
        }
    }
}
