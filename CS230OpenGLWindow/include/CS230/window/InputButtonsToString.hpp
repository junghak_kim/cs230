/*
 * Rudy Castan
 * Windows Library
 * CS230
 */
#pragma once
#include "InputButtons.hpp"

namespace CS230
{
    constexpr const char* to_string(MouseButton button) noexcept;
    constexpr const char* to_string(KeyboardButton button) noexcept;

    constexpr const wchar_t* to_wstring(MouseButton button) noexcept;
    constexpr const wchar_t* to_wstring(KeyboardButton button) noexcept;
}

#include "InputButtonsToString.inl"
