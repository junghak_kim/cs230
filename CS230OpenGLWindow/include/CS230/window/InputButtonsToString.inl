#pragma once

namespace CS230
{
    constexpr const char* to_string(MouseButton button) noexcept
    {
        switch (button)
        {
            case MouseButton::None:
                return "None";
            case MouseButton::Left:
                return "Left";
            case MouseButton::Middle:
                return "Middle";
            case MouseButton::Right:
                return "Right";
            case MouseButton::Count:
                return "Count";
            default:
                return "None";
        }
    }

    constexpr const char* to_string(KeyboardButton button) noexcept
    {
        switch (button)
        {
            case KeyboardButton::None:
                return "None";
            case KeyboardButton::Escape:
                return "Escape";
            case KeyboardButton::Space:
                return "Space";
            case KeyboardButton::Left:
                return "Left";
            case KeyboardButton::Up:
                return "Up";
            case KeyboardButton::Right:
                return "Right";
            case KeyboardButton::Down:
                return "Down";
            case KeyboardButton::_0:
                return "0";
            case KeyboardButton::_1:
                return "1";
            case KeyboardButton::_2:
                return "2";
            case KeyboardButton::_3:
                return "3";
            case KeyboardButton::_4:
                return "4";
            case KeyboardButton::_5:
                return "5";
            case KeyboardButton::_6:
                return "6";
            case KeyboardButton::_7:
                return "7";
            case KeyboardButton::_8:
                return "8";
            case KeyboardButton::_9:
                return "9";
            case KeyboardButton::A:
                return "A";
            case KeyboardButton::B:
                return "B";
            case KeyboardButton::C:
                return "C";
            case KeyboardButton::D:
                return "D";
            case KeyboardButton::E:
                return "E";
            case KeyboardButton::F:
                return "F";
            case KeyboardButton::G:
                return "G";
            case KeyboardButton::H:
                return "H";
            case KeyboardButton::I:
                return "I";
            case KeyboardButton::J:
                return "J";
            case KeyboardButton::K:
                return "K";
            case KeyboardButton::L:
                return "L";
            case KeyboardButton::M:
                return "M";
            case KeyboardButton::N:
                return "N";
            case KeyboardButton::O:
                return "O";
            case KeyboardButton::P:
                return "P";
            case KeyboardButton::Q:
                return "Q";
            case KeyboardButton::R:
                return "R";
            case KeyboardButton::S:
                return "S";
            case KeyboardButton::T:
                return "T";
            case KeyboardButton::U:
                return "U";
            case KeyboardButton::V:
                return "V";
            case KeyboardButton::W:
                return "W";
            case KeyboardButton::X:
                return "X";
            case KeyboardButton::Y:
                return "Y";
            case KeyboardButton::Z:
                return "Z";
            case KeyboardButton::NumPad_0:
                return "NumPad_0";
            case KeyboardButton::NumPad_1:
                return "NumPad_1";
            case KeyboardButton::NumPad_2:
                return "NumPad_2";
            case KeyboardButton::NumPad_3:
                return "NumPad_3";
            case KeyboardButton::NumPad_4:
                return "NumPad_4";
            case KeyboardButton::NumPad_5:
                return "NumPad_5";
            case KeyboardButton::NumPad_6:
                return "NumPad_6";
            case KeyboardButton::NumPad_7:
                return "NumPad_7";
            case KeyboardButton::NumPad_8:
                return "NumPad_8";
            case KeyboardButton::NumPad_9:
                return "NumPad_9";
            case KeyboardButton::Count:
                return "Count";
            default:
                return "None";
        }
    }

    constexpr const wchar_t* to_wstring(MouseButton button) noexcept
    {
        switch (button)
        {
            case MouseButton::None:
                return L"None";
            case MouseButton::Left:
                return L"Left";
            case MouseButton::Middle:
                return L"Middle";
            case MouseButton::Right:
                return L"Right";
            case MouseButton::Count:
                return L"Count";
            default:
                return L"None";
        }
    }

    constexpr const wchar_t* to_wstring(KeyboardButton button) noexcept
    {
        switch (button)
        {
            case KeyboardButton::None:
                return L"None";
            case KeyboardButton::Escape:
                return L"Escape";
            case KeyboardButton::Space:
                return L"Space";
            case KeyboardButton::Left:
                return L"Left";
            case KeyboardButton::Up:
                return L"Up";
            case KeyboardButton::Right:
                return L"Right";
            case KeyboardButton::Down:
                return L"Down";
            case KeyboardButton::_0:
                return L"0";
            case KeyboardButton::_1:
                return L"1";
            case KeyboardButton::_2:
                return L"2";
            case KeyboardButton::_3:
                return L"3";
            case KeyboardButton::_4:
                return L"4";
            case KeyboardButton::_5:
                return L"5";
            case KeyboardButton::_6:
                return L"6";
            case KeyboardButton::_7:
                return L"7";
            case KeyboardButton::_8:
                return L"8";
            case KeyboardButton::_9:
                return L"9";
            case KeyboardButton::A:
                return L"A";
            case KeyboardButton::B:
                return L"B";
            case KeyboardButton::C:
                return L"C";
            case KeyboardButton::D:
                return L"D";
            case KeyboardButton::E:
                return L"E";
            case KeyboardButton::F:
                return L"F";
            case KeyboardButton::G:
                return L"G";
            case KeyboardButton::H:
                return L"H";
            case KeyboardButton::I:
                return L"I";
            case KeyboardButton::J:
                return L"J";
            case KeyboardButton::K:
                return L"K";
            case KeyboardButton::L:
                return L"L";
            case KeyboardButton::M:
                return L"M";
            case KeyboardButton::N:
                return L"N";
            case KeyboardButton::O:
                return L"O";
            case KeyboardButton::P:
                return L"P";
            case KeyboardButton::Q:
                return L"Q";
            case KeyboardButton::R:
                return L"R";
            case KeyboardButton::S:
                return L"S";
            case KeyboardButton::T:
                return L"T";
            case KeyboardButton::U:
                return L"U";
            case KeyboardButton::V:
                return L"V";
            case KeyboardButton::W:
                return L"W";
            case KeyboardButton::X:
                return L"X";
            case KeyboardButton::Y:
                return L"Y";
            case KeyboardButton::Z:
                return L"Z";
            case KeyboardButton::NumPad_0:
                return L"NumPad_0";
            case KeyboardButton::NumPad_1:
                return L"NumPad_1";
            case KeyboardButton::NumPad_2:
                return L"NumPad_2";
            case KeyboardButton::NumPad_3:
                return L"NumPad_3";
            case KeyboardButton::NumPad_4:
                return L"NumPad_4";
            case KeyboardButton::NumPad_5:
                return L"NumPad_5";
            case KeyboardButton::NumPad_6:
                return L"NumPad_6";
            case KeyboardButton::NumPad_7:
                return L"NumPad_7";
            case KeyboardButton::NumPad_8:
                return L"NumPad_8";
            case KeyboardButton::NumPad_9:
                return L"NumPad_9";
            case KeyboardButton::Count:
                return L"Count";
            default:
                return L"None";
        }
    }
}
