/*
 * Rudy Castan
 * Windows Library
 * CS230
 */
#pragma once

namespace CS230
{
    enum class MouseButton
    {
        None,
        Left,
        Middle,
        Right,
        Count
    };

    enum class KeyboardButton
    {
        None,
        Escape,
        Space,
        Left,
        Up,
        Right,
        Down,
        _0,
        _1,
        _2,
        _3,
        _4,
        _5,
        _6,
        _7,
        _8,
        _9,
        A,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
        I,
        J,
        K,
        L,
        M,
        N,
        O,
        P,
        Q,
        R,
        S,
        T,
        U,
        V,
        W,
        X,
        Y,
        Z,
        NumPad_0,
        NumPad_1,
        NumPad_2,
        NumPad_3,
        NumPad_4,
        NumPad_5,
        NumPad_6,
        NumPad_7,
        NumPad_8,
        NumPad_9,
        Count
    };
}
