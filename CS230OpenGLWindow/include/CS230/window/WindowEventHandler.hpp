/*
 * Rudy Castan
 * Windows Library
 * CS230
 */
#pragma once
namespace CS230
{
    namespace window
    {
        struct size;
    }

    enum class KeyboardButton;
    enum class MouseButton;

    class WindowEventHandler
    {
    public:
        virtual void HandleKeyPress(KeyboardButton button)             = 0;
        virtual void HandleKeyReleased(KeyboardButton button)          = 0;
        virtual void HandleMouseButtonPress(MouseButton button)        = 0;
        virtual void HandleMouseButtonReleased(MouseButton button)     = 0;
        virtual void HandleMouseWheelScroll(int scroll_amount)         = 0;
        virtual void HandleMouseMove(int mouse_x, int mouse_y)         = 0;
        virtual void HandleResizeEvent(const window::size& frame_size) = 0;
        virtual void HandleWindowClose()                               = 0;
    };

    /**
     * \brief A simple base class you can use that provides an empty default implementation for all of the events
     */
    class SimpleWindowEventHandler : public WindowEventHandler
    {
    public:
        void HandleKeyPress(KeyboardButton /*button*/) override {}
        void HandleKeyReleased(KeyboardButton /*button*/) override {}
        void HandleMouseButtonPress(MouseButton /*button*/) override {}
        void HandleMouseButtonReleased(MouseButton /*button*/) override {}
        void HandleMouseWheelScroll(int /*scroll_amount*/) override {}
        void HandleMouseMove(int /*mouse_x*/, int /*mouse_y*/) override {}
        void HandleResizeEvent(const window::size& /*frame_size*/) override {}
        void HandleWindowClose() override {}
    };
}
