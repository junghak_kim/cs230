#pragma once
#include <CS230/graphics/BitmapFont.hpp>

namespace CS230
{
    namespace DEFAULTS
    {
        BitmapFont bitmap_font_sansation() noexcept;
    };
    
}
