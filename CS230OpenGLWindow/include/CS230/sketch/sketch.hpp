/*
 * Rudy Castan
 * sketch Library
 * CS230
 */
#pragma once
#include <CS230/graphics/BitmapFont.hpp>
#include <CS230/graphics/CameraView.hpp>
#include <CS230/graphics/Color4f.hpp>
#include <CS230/graphics/Color4ub.hpp>
#include <CS230/graphics/Text.hpp>
#include <CS230/graphics/Vertices.hpp>
#include <CS230/graphics/material.hpp>
#include <CS230/math/vector2.hpp>
#include <stack>
#include <string>

namespace CS230
{
    class BitmapFont;

    class [[nodiscard]] Sketch
    {
    public:
        void SetupSketch(int camera_pixel_width, int camera_pixel_height) noexcept;

        void ClearScreen() const noexcept;
        void ClearScreen(HexColor color) const noexcept;
        void ClearScreen(Color4ub::unsigned_byte grey, Color4ub::unsigned_byte alpha = 0xff) const noexcept;
        void ClearScreen(Color4ub::unsigned_byte red, Color4ub::unsigned_byte green, Color4ub::unsigned_byte blue,
                         Color4ub::unsigned_byte alpha = 0xff) const noexcept;

        void SetFillColor(HexColor color) noexcept;
        void SetFillColor(Color4ub::unsigned_byte grey, Color4ub::unsigned_byte alpha = 0xff) noexcept;
        void SetFillColor(Color4ub::unsigned_byte red, Color4ub::unsigned_byte green, Color4ub::unsigned_byte blue,
                          Color4ub::unsigned_byte alpha = 0xff) noexcept;
        void NoFill() noexcept;
        void SetOutlineColor(HexColor color) noexcept;
        void SetOutlineColor(Color4ub::unsigned_byte grey, Color4ub::unsigned_byte alpha = 0xff) noexcept;
        void SetOutlineColor(Color4ub::unsigned_byte red, Color4ub::unsigned_byte green, Color4ub::unsigned_byte blue,
                             Color4ub::unsigned_byte alpha = 0xff) noexcept;
        void NoOutline();
        void SetOutlineWidth(float line_width) noexcept;

        void DrawText(const std::wstring& str, float x, float y) noexcept;
        void DrawText(const std::string& str, float x, float y) noexcept;
        void DrawText(const std::wstring& str, vector2 position) noexcept;
        void DrawText(const std::string& str, vector2 position) noexcept;
        void SetTextFont(BitmapFont && bitmap_font) noexcept;

        void DrawEllipse(vector2 position, vector2 size) noexcept;
        void DrawEllipse(float x, float y, float width, float height = 0) noexcept;
        void DrawEllipse(vector2 position, float width, float height = 0) noexcept;
        void DrawEllipse(float x, float y, vector2 size) noexcept;
        enum class EllipseMode
        {
            Center,
            Corner
        };
        void SetEllipseMode(EllipseMode mode) noexcept;

        void DrawLine(vector2 position1, vector2 position2) noexcept;
        void DrawLine(float x1, float y1, float x2, float y2) noexcept;

        void DrawQuad(vector2 position1, vector2 position2, vector2 position3, vector2 position4) noexcept;
        void DrawQuad(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) noexcept;

        void DrawRect(vector2 position, vector2 size) noexcept;
        void DrawRect(float x, float y, float width, float height = 0) noexcept;
        void DrawRect(vector2 position, float width, float height = 0) noexcept;
        void DrawRect(float x, float y, vector2 size) noexcept;
        enum class RectMode
        {
            Corner,
            Center
        };
        void SetRectMode(RectMode mode) noexcept;

        void DrawTriangle(vector2 position1, vector2 position2, vector2 position3) noexcept;
        void DrawTriangle(float x1, float y1, float x2, float y2, float x3, float y3) noexcept;

        void SetCameraViewSize(vector2 pixel_size) noexcept;
        void SetCameraViewSize(int new_pixel_width, int new_pixel_height) noexcept;

        void SetFrameOfReference(CameraView::FrameOfReference frame_of_reference) noexcept;

        void PushMatrix() noexcept;
        void ApplyScale(float scale_x, float scale_y) noexcept;
        void ApplyScale(float scale) noexcept;
        void ApplyScale(vector2 scale) noexcept;
        void ApplyRotate(float angle_in_radians) noexcept;
        void ApplyTranslate(float translate_x, float translate_y) noexcept;
        void ApplyTranslate(vector2 translation) noexcept;
        void ApplyMatrix(const matrix3& matrix) noexcept;
        void PopMatrix() noexcept;

        void FinishSketchingFrame() noexcept;

    private:
        void Draw(Shader * shader, const Vertices& vertices, const matrix3& model_to_world) noexcept;
        void Draw(Shader * shader, const Vertices& vertices) noexcept;

    private:
        EllipseMode         ellipseMode  = EllipseMode::Center;
        RectMode            rectMode     = RectMode::Corner;
        Color4f             fillColor    = {1.0f};
        Color4f             outlineColor = {0.0f};
        bool                drawFill     = true;
        bool                drawOutline  = true;
        std::stack<matrix3> matrixStack{};

        CameraView view{};
        material   material{};

        Vertices   ellipseCenter{};
        Vertices   ellipseCenterOutline{};
        Vertices   ellipseCorner{};
        Vertices   ellipseCornerOutline{};
        Vertices   rectCorner{};
        Vertices   rectCornerOutline{};
        Vertices   rectCenter{};
        Vertices   rectCenterOutline{};
        Vertices   line{};
        Vertices   quad{};
        Vertices   quadOutline{};
        Vertices   triangle{};
        Vertices   triangleOutline{};
        BitmapFont font;
        Text       text;
        float      depth = 0.0f;
    };
}
