﻿#include "Demo.hpp"
#include <CS230/window/InputButtonsToString.hpp>
#include <array>
#include <iostream>

bool Demo::StartUpWasSuccessful()
{
    sketch.SetupSketch(width, height);
    sketch.SetFrameOfReference(CS230::CameraView::LeftHanded_OriginTopLeft);
    return true;
}

bool Demo::ShouldClose() const { return isDone; }

void Demo::Update(float delta_time_seconds)
{
    ++frameCount;
    seconds += delta_time_seconds;

    fpsElapsedTime += delta_time_seconds;
    ++fpsFrames;
    if (fpsElapsedTime >= 1.0f)
    {
        fpsText        = L"fps: " + std::to_wstring(int(fpsFrames / fpsElapsedTime));
        fpsElapsedTime = 0;
        fpsFrames      = 0;
    }

    using std::to_string;
    using namespace CS230;
    displayString.clear();
    displayString += "Keys pressed:";
    for (auto button : pressedKeyboardButtons)
    {
        displayString += " ";
        displayString += to_string(button);
        displayString += ",";
    }

    displayString += "\nLast Button Released: ";
    displayString += to_string(lastReleasedKeyButton);

    displayString += "\nMouse Buttons Pressed: ";
    for (auto button : pressedMouseButtons)
    {
        displayString += " ";
        displayString += to_string(button);
        displayString += ",";
    }

    displayString += "\nLast Mouse Button Released: ";
    displayString += to_string(lastReleasedMouseButton);

    displayString += "\nMouse Scroll Wheel Speed: " + to_string(scrollSpeed);

    displayString += "\nMouse Screen Position: (" + to_string(mouseX) + ", " + to_string(mouseY) + ")";

    displayString +=
        "\nPrevious Mouse Position: (" + to_string(previousMouseX) + ", " + to_string(previousMouseY) + ")";
}

void Demo::Draw()
{
    sketch.ClearScreen();

    float y_offset = 0;
    sketch.DrawText(fpsText, 0, y_offset += 32);
    sketch.DrawText(std::string("Screen Size: (") + std::to_string(width) + ", " + std::to_string(height) + ")", 0,
                    y_offset += 32);
    sketch.DrawText(std::string("FrameCount: ") + std::to_string(frameCount), 0, y_offset += 32);
    sketch.DrawText(std::string("Seconds: ") + std::to_string(seconds), 0, y_offset += 32);

    sketch.DrawText(displayString, 0, y_offset += 32);

    sketch.FinishSketchingFrame();
}

void Demo::ShutDown() {}

void Demo::HandleKeyPress(CS230::KeyboardButton keyboard_button)
{
    pressedKeyboardButtons.push_back(keyboard_button);
    std::cout << "KeyPress: " << CS230::to_string(keyboard_button) << "\n";
}

void Demo::HandleKeyReleased(CS230::KeyboardButton keyboard_button)
{
    lastReleasedKeyButton = keyboard_button;
    pressedKeyboardButtons.erase(
        std::remove(pressedKeyboardButtons.begin(), pressedKeyboardButtons.end(), keyboard_button),
        pressedKeyboardButtons.end());
    std::cout << "KeyRelease: " << CS230::to_string(keyboard_button) << "\n";
}

void Demo::HandleMouseButtonPress(CS230::MouseButton mouse_button)
{
    pressedMouseButtons.push_back(mouse_button);
    std::cout << "MousePress: " << CS230::to_string(mouse_button) << "\n";
}

void Demo::HandleMouseButtonReleased(CS230::MouseButton mouse_button)
{
    lastReleasedMouseButton = mouse_button;
    pressedMouseButtons.erase(std::remove(pressedMouseButtons.begin(), pressedMouseButtons.end(), mouse_button),
                              pressedMouseButtons.end());
    std::cout << "MouseRelease: " << CS230::to_string(mouse_button) << "\n";
}

void Demo::HandleMouseWheelScroll(int scroll_amount)
{
    scrollSpeed = scroll_amount;
    std::cout << "MouseScroll: " << scroll_amount << "\n";
}

void Demo::HandleMouseMove(int mouse_x, int mouse_y)
{
    previousMouseX = mouseX;
    previousMouseY = mouseY;
    mouseX         = mouse_x;
    mouseY         = mouse_y;
    std::cout << "MousePosition: (" << mouse_x << ", " << mouse_y << ")\n";
}

void Demo::HandleResizeEvent(CS230::window::size size)
{
    width  = size.width;
    height = size.height;
    sketch.SetCameraViewSize(width, height);
    std::cout << "Resize: (" << width << ", " << height << ")\n";
}
