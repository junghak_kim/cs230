﻿#pragma once
#include "Demo.hpp"
#include <CS230/util/Timer.hpp>
#include <CS230/window/OpenGLWindow.hpp>
#include <CS230/window/WindowEventHandler.hpp>
#include <string>

namespace CS230
{
    enum class KeyboardButton;
    enum class MouseButton;
}

class Application : public CS230::WindowEventHandler
{
public:
    bool StartUpWasSuccessful(std::string app_name, CS230::window::size window_dimensions = {0, 0});
    bool ShouldClose() const;
    void Update();
    void ShutDown();

public:
    void HandleKeyPress(CS230::KeyboardButton button) override;
    void HandleKeyReleased(CS230::KeyboardButton button) override;
    void HandleMouseButtonPress(CS230::MouseButton button) override;
    void HandleMouseButtonReleased(CS230::MouseButton button) override;
    void HandleMouseWheelScroll(int scroll_amount) override;
    void HandleMouseMove(int mouse_x, int mouse_y) override;
    void HandleResizeEvent(const CS230::window::size& frame_size) override;
    void HandleWindowClose() override;

private:
    CS230::Timer        timer{};
    CS230::OpenGLWindow window{};
    Demo                logic{};
};
