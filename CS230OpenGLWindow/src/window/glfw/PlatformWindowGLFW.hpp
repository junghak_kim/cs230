#pragma once
#include <CS230/window/OpenGLWindow.hpp>
#include <GLFW/glfw3.h>


namespace CS230
{

    class PlatformWindow
    {
    public:

		GLFWwindow* window;
		bool CreateAndShowWindow(const char* title, WindowEventHandler* event_handler,
			window::size desired_size,
			OpenGLWindow::FullScreen   start_fullscreen_as) noexcept;
		void PollEvents() const noexcept;
		bool ShouldQuit() const noexcept;
		void ShutDown() const noexcept;
		void SwapBackBuffer() const noexcept;
		void SetWindowTitle(const char* new_title) const noexcept;
		void TurnOnMonitorVerticalSynchronization(bool enable) const noexcept;
		bool IsMonitorVerticalSynchronizationOn() const noexcept;
		bool IsFullScreen() const noexcept;
		void ToggleFullScreen() const noexcept;
    };

}
