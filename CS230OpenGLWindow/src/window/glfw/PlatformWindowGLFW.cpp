#include "PlatformWindowGLFW.hpp"
#include <CS230/window/InputButtons.hpp>
#include <CS230/window/WindowEventHandler.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>


void error_callback(int error, const char* description)
{
	error;
	fprintf(stderr, "Error: %s\n", description);
}

namespace CS230
{
	bool PlatformWindow::CreateAndShowWindow(const char* title, WindowEventHandler* event_handler,
		window::size desired_size, OpenGLWindow::FullScreen start_fullscreen_as) noexcept
	{
		event_handler; start_fullscreen_as; desired_size;
		glfwSetErrorCallback(error_callback);

		if (!glfwInit())
		{
			glfwTerminate();
			std::exit(EXIT_FAILURE);
		}

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

		window = glfwCreateWindow(800, 600, title, nullptr, nullptr);
		if(!window)
		{
			glfwTerminate();
			std::exit(EXIT_FAILURE);
		}
		glfwMakeContextCurrent(window);
		GLenum err = glewInit();
		if (err != GLEW_OK) {
			std::cerr << glewGetErrorString(err);
			std::exit(EXIT_FAILURE);
		}

		return true;
	}

	void PlatformWindow::PollEvents() const noexcept
	{
	}

	bool PlatformWindow::ShouldQuit() const noexcept
	{
		return false;
	}

	void PlatformWindow::ShutDown() const noexcept
	{
		glfwDestroyWindow(window);
	}

	void PlatformWindow::SwapBackBuffer() const noexcept
	{
		glfwSwapBuffers(window);
	}

	void PlatformWindow::SetWindowTitle(const char* new_title) const noexcept
	{
		new_title;
	}

	void PlatformWindow::TurnOnMonitorVerticalSynchronization(bool enable) const noexcept
	{
		enable;
	}

	bool PlatformWindow::IsMonitorVerticalSynchronizationOn() const noexcept
	{
		return true;
	}

	bool PlatformWindow::IsFullScreen() const noexcept
	{
		return false;
	}

	void PlatformWindow::ToggleFullScreen() const noexcept
	{
	}
}
