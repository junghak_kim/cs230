﻿#pragma once
#include <CS230/sketch/sketch.hpp>
#include <CS230/window/InputButtons.hpp>
#include <CS230/window/WindowSize.hpp>
#include <vector>

class Demo
{
public:
    bool StartUpWasSuccessful();
    bool ShouldClose() const;
    void Update(float delta_time_seconds);
    void Draw();
    void ShutDown();

    void HandleKeyPress(CS230::KeyboardButton keyboard_button);
    void HandleKeyReleased(CS230::KeyboardButton keyboard_button);
    void HandleMouseButtonPress(CS230::MouseButton mouse_button);
    void HandleMouseButtonReleased(CS230::MouseButton mouse_button);
    void HandleMouseWheelScroll(int scroll_amount);
    void HandleMouseMove(int mouse_x, int mouse_y);
    void HandleResizeEvent(CS230::window::size size);

private:
    int                                mouseX         = 0;
    int                                mouseY         = 0;
    int                                previousMouseX = 0;
    int                                previousMouseY = 0;
    int                                width          = 800;
    int                                height         = 600;
    int                                scrollSpeed    = 0;
    std::vector<CS230::KeyboardButton> pressedKeyboardButtons{};
    std::vector<CS230::MouseButton>    pressedMouseButtons{};
    CS230::MouseButton                 lastReleasedMouseButton = CS230::MouseButton::None;
    CS230::KeyboardButton              lastReleasedKeyButton   = CS230::KeyboardButton::None;


    float seconds    = 0;
    int   frameCount = 0;
    bool  isDone     = false;

    std::wstring fpsText{};
    float        fpsElapsedTime = 0;
    int          fpsFrames      = 0;

    CS230::Sketch sketch{};

    std::string displayString{};
};
