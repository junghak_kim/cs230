#include "Application.hpp"

int main(int, const char**)
{
    Application application;
    if (!application.StartUpWasSuccessful("CS230 OpenGL Window Assignment"))
        return -1;

    while (!application.ShouldClose())
        application.Update();

    return 0;
}
